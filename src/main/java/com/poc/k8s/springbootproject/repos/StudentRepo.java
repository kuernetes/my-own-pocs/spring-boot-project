package com.poc.k8s.springbootproject.repos;

import com.poc.k8s.springbootproject.entities.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepo extends CrudRepository<Student, Long> {
}
