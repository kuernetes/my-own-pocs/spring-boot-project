package com.poc.k8s.springbootproject.responses;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StudentResponse {
    private Long id;
    private String name;
}
