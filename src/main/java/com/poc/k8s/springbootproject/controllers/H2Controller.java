package com.poc.k8s.springbootproject.controllers;

import com.poc.k8s.springbootproject.entities.Student;
import com.poc.k8s.springbootproject.repos.StudentRepo;
import com.poc.k8s.springbootproject.requests.CreateStudentRequest;
import com.poc.k8s.springbootproject.responses.StudentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "h2")
public class H2Controller {

    private final StudentRepo studentRepo;

    public H2Controller(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    @PostMapping
    public ResponseEntity<StudentResponse> save(@RequestBody CreateStudentRequest request){
        Student savedStudent = studentRepo.save(new Student(request.getName()));
        StudentResponse response = new StudentResponse();
        response.setId(savedStudent.getId());
        response.setName(savedStudent.getName());
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(response);
    }

    @GetMapping
    public ResponseEntity<Iterable<StudentResponse>> getAll(){
        List<StudentResponse> allStudentsRes = new ArrayList<>();
        Iterable<Student> allStudents = studentRepo.findAll();
        allStudents.forEach(s ->{
            StudentResponse response = new StudentResponse();
            response.setId(s.getId());
            response.setName(s.getName());
            allStudentsRes.add(response);
        });
        return ResponseEntity.ok(allStudentsRes);
    }

    @GetMapping("{id}")
    public ResponseEntity<StudentResponse> getAll(@PathVariable Long id){
        Optional<Student> oStudentResponse = studentRepo.findById(id);
        StudentResponse response = new StudentResponse();
        if(oStudentResponse.isPresent()){
            Student studentResponse = oStudentResponse.get();
            response.setId(studentResponse.getId());
            response.setName(studentResponse.getName());
        }
        return ResponseEntity.ok(response);
    }
}
