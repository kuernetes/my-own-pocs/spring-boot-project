package com.poc.k8s.springbootproject.controllers;

import com.poc.k8s.springbootproject.responses.StudentResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("hc")
public class HardCodedController {

    @GetMapping
    public ResponseEntity<StudentResponse> get(){
        StudentResponse response = new StudentResponse();
        response.setId(1l);
        response.setName("Hardcoded name");
        return ResponseEntity.<StudentResponse>ok(response);
    }
}
