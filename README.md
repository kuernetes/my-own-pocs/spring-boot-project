### spring-boot-project 

```info
- This branch has API for hard coded response and H2 DB integration.
- It includes Dockerfile to build image for this project and run it.
```

#### Steps to Run API in Docker container

##### Prequisite

```Prequisite
- docker must be installed
```
##### Steps

```step
s1 - clone this branch
s2 - run "cd spring-boot-project"
s3 - run "mvn clean install -DskipTests"
s4 - run "docker build -t k8s-poc-spring-boot-project:h2 . "
s5 - run "docker run -d -p 8080:8080 k8s-poc-spring-boot-project:h2 "

THIS WILL START API SERVER IN DOCKER CONTAINER
```
##### Access APIs

```
[h2-console]
- basePath - http://localhost:8080/spring-boot-project
{{basePath}}/spring-boot-project/h2-console/spring-boot-project/h2-console
  - Pass below details to connect to h2 db
    -- spring.datasource.url=jdbc:h2:mem:testdb 
    -- spring.datasource.username=admin
    -- spring.datasource.password=admin

[hardcoded]
- refer - ./postman/<postman-collection>

[h2-jpa]
- refer - ./postman/<postman-collection>
```

#### Steps to Run API in k8s - using NodePort

##### Prequisite
```prequisite
- docker must be installed 
- under docker desktop setting - enable k8s auto start at the time of docker start
```
##### Steps

```step
s1 - clone this branch
s2 - run "cd spring-boot-project"
s3 - kubectl apply -f k8s

THIS WILL START API SERVER POD AND IT IS ACCESS BY K8S SERVICE [NODEPORT]
```
#### Access APIs

```
- basePath - http://localhost:31521/spring-boot-project
[h2-console]
{{basePath}}/spring-boot-project/h2-console
  - Pass below details to connect to h2 db
    -- spring.datasource.url=jdbc:h2:mem:testdb 
    -- spring.datasource.username=admin
    -- spring.datasource.password=admin

[hardcoded]
- refer - ./postman/<postman-collection>

[h2-jpa]
- refer - ./postman/<postman-collection>
``` 
